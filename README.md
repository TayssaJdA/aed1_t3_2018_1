# AED1 - Trabalho 3 - Árvore AVL

Declaro que o presente trabalho contém código desenvolvido exclusivamente por mim e que não foi compartilhado de nenhuma forma com terceiros a não ser o professor da disciplina. Compreendo que qualquer violação destes princípios será punida rigorosamente de acordo com o Regimento da UFPEL.

(Preencha com seus dados)

- Nome completo: Tayssa Jardim de Avila
- Username do Bitbucket: TayssaJdA
- Email @inf: tjdavila@inf.ufpel.edu.br


## Descrição 

Este trabalho consiste no desenvolvimento de código para uma árvore binária de pesquisa AVL que armazena valores inteiros. As funções a serem implementadas estão declaradas em *arvore.h*.

Os testes dos alunos deverão ser documentados e incluir o valor esperado da saída.

## Produtos

- aluno.c: testes do aluno (2 pontos)
- arvore.c: implementação das funções da árvore (8 pontos)

A versão final deverá executar os testes dos Professores no Bitbucket usando a regra _make_. 

O Makefile poderá ser modificado para adicionar as regras para construir as bibliotecas de listas, filas e pilhas que serão copiadas do repositório do aluno. 



## Cronograma

- Entrega final: _20/07/2018_


