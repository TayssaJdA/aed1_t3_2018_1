#include "arvore.h"
#include <stdio.h>
#include<stdlib.h>

	struct arvore{
	struct nodo *raiz;
};


struct nodo *rightRotate(struct nodo *y)
{
    struct nodo *x = y->esq;
    struct nodo *T2 = x->dir;
 
    x->dir = y;
    y->esq = T2;

    return x;
}
 
struct nodo *leftRotate(struct nodo *x)
{
    struct nodo *y = x->dir;
    struct nodo *T2 = y->esq;
 
    y->esq = x;
    x->dir = T2;
 
    return y;
}

struct nodo* findMin(struct nodo* raiz){
	if(raiz->esq != NULL){
		return findMin(raiz->esq);
	}else{
		return raiz;
	}
}


struct nodo * inicializa_arvore(int entradas, int * valores){
	int i = 0;
	struct arvore *novo;
	novo->raiz= NULL;
	while(i<entradas){
			novo->raiz = insere_nodo(novo->raiz, valores[i]);
		i++;
	}
	return novo->raiz;	
}	

struct nodo* insere_nodo(struct nodo* raiz, int valor)
{
    
    if (raiz == NULL)
        return 0;
 
    if (valor < raiz->valor)
        raiz->esq  = insere_nodo(raiz->esq, valor);
    else if (valor > raiz->valor)
        raiz->dir = insere_nodo(raiz->dir, valor);
    else 
        return raiz;
 
 
    raiz->balanceamento = balanceada(raiz);
 
    if (raiz->balanceamento > 1 && valor < raiz->esq->valor)
        return rightRotate(raiz);

    if (raiz->balanceamento < -1 && valor > raiz->dir->valor)
        return leftRotate(raiz);
 
    if (raiz->balanceamento > 1 && valor > raiz->esq->valor)
    {
        raiz->esq =  leftRotate(raiz->esq);
        return rightRotate(raiz);
    }
 
    if (raiz->balanceamento < -1 && valor < raiz->dir->valor)
    {
        raiz->dir = rightRotate(raiz->dir);
        return leftRotate(raiz);
    }
 
    return raiz;
}

int altura(struct nodo * raiz){
	int esq, dir;
	if(raiz == NULL){
		return 0;
	}
	esq = altura(raiz->esq);
	dir = altura(raiz->dir);
	if(esq > dir){
		return esq + 1;
	}else {
		return dir + 1;
	}
}

struct nodo * remove_nodo(struct nodo * raiz, int valor){
	struct nodo *aux;
	if(raiz == NULL){
		return NULL;
	} else if(raiz->valor > valor){
		raiz->esq = remove_nodo(raiz->esq, valor);
		return 0;
		} else if(raiz->valor < valor){
			raiz->dir = remove_nodo(raiz->dir, valor);
			return 0;
		}else if(raiz->dir != NULL && raiz->esq != NULL){
			aux = findMin(raiz->dir);
			raiz->valor = aux->valor;
			raiz->dir = remove_nodo(raiz->dir, raiz->valor);
		} else if(raiz->esq == NULL){
			aux = raiz->dir;
		}else {
			aux = raiz->esq;
		}	
	free(raiz);
		
 
    raiz->balanceamento = balanceada(aux);
 
    if (raiz->balanceamento > 1 && valor < aux->esq->valor)
        return rightRotate(aux);

    if (raiz->balanceamento < -1 && valor > aux->dir->valor)
        return leftRotate(aux);
 
    if (raiz->balanceamento > 1 && valor > aux->esq->valor)
    {
        aux->esq =  leftRotate(aux->esq);
        return rightRotate(aux);
    }
 
    if (raiz->balanceamento < -1 && valor < aux->dir->valor)
    {
        aux->dir = rightRotate(aux->dir);
        return leftRotate(aux);
    }
 
    return aux;
}
	

struct nodo * busca(struct nodo * raiz, int valor){
	if(raiz==NULL){
		return NULL;
	}else if(raiz->valor = valor){
		return raiz;
	}else if(raiz->valor > valor){
		return busca(raiz->esq, valor);
	}else {
		return busca(raiz->dir, valor);
	}
}

int numero_elementos(struct nodo * raiz){
	if(raiz == NULL){
		return 0;
	}else {
		return numero_elementos(raiz->esq) + numero_elementos(raiz->dir) + 1;
	}
}



int balanceada(struct nodo * raiz){
	if(raiz == NULL){
		return 0;
	}else {
	return altura(raiz->esq) - altura(raiz->dir);
	}
}

void imprime(int * valores, int tamanho){
	int i;
    for(i = 0; i < tamanho; i ++){
    	printf("%d ", valores[i]);
	}
}

void remove_todos(struct nodo * raiz){
	if(raiz != NULL){
		libera_nodo(raiz);
		free(raiz);
	}
}


void libera_nodo(struct nodo *no){
	if(no != NULL){
		libera_nodo(no->esq);
		libera_nodo(no->dir);
		free(no);
		no = NULL;
	}
}

int prefix(struct nodo *raiz, int *resultado)
{
    if(raiz != NULL)
    {
    	int i = 0;
        resultado[i] = raiz->valor;
        i++;
        prefix(raiz->esq, resultado);
        prefix(raiz->dir, resultado);
        return i;
    }else return 0;
}

int infix(struct nodo* raiz, int *resultado){
	    if(raiz != NULL)
    {
    	int i = 0;
        prefix(raiz->esq, resultado);
        resultado[i] = raiz->valor;
        i++;
        prefix(raiz->dir, resultado);
        return i;
    }else return 0;
}

int postfix(struct nodo* raiz, int *resultado){
	    if(raiz != NULL)
    {
    	int i = 0;
        prefix(raiz->esq, resultado);
        prefix(raiz->dir, resultado);
        resultado[i] = raiz->valor;
        i++;
        return i;
    } else return 0;
}

int percorre_nivel(struct nodo* raiz, int nivel)
{
  if(raiz == NULL)
     return 0;
  else{
  	if(nivel == 1){
  	return raiz->valor;
  }
  else if (nivel > 1)
  {
    return percorre_nivel(raiz->esq, nivel-1);
    return percorre_nivel(raiz->dir, nivel-1);
  }
  }
}

int abrangencia(struct nodo * raiz, int * resultado){
	if(raiz != NULL){
		int i = 0;
		int x;
		x = altura(raiz);
		for(i=0; i<x; i++){
			resultado[i] = percorre_nivel(raiz, i);
		}
		return i;
	} else return 0;
}

